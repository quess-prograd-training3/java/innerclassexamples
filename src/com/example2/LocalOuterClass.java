package com.example2;

public class LocalOuterClass {
    public void outerDisplay(){
        class LocalInnerClass{
            public void printMessageInner(){
                System.out.println("this is local innerclass");
            }
        }
        LocalInnerClass localInnerClassObject = new LocalInnerClass();
        localInnerClassObject.printMessageInner();
    }
}
