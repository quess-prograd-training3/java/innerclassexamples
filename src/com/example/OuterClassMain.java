package com.example;

public class OuterClassMain {
    public static void main(String[] args) {
        OuterClassExample outerClassExample = new OuterClassExample();
        // OuterClassName.InnerClassName innerClassObject = outerClassObject.new InnerClassName();
        OuterClassExample.Display innerClassObject = outerClassExample.new Display();
        innerClassObject.message();
    }
}
