package com.example;

public class OuterClassExample {
    // two types of inner classes
    //1. static inner class
    //2. non - static inner class
    //static inner class is called as nested inner class
    //non static inner class is called as simply inner class
    //three types in non static inner class
    //1. member inner class
    //2. local inner class
    //3.anonymous inner class
    //nested classes to separate the things logicall and to make the things readiable
    int firstNumber;
    int secondNumber;
    public OuterClassExample() {
        this.firstNumber=10;
        this.secondNumber=20;
    }
    public void displayOuter(){
        // System.out.println(var);
    }
    public class Display{
        int var;
        public Display() {
            this.var = 30;
        }
        public void message(){
            System.out.println("display inner/member class");
            System.out.println(firstNumber);
            System.out.println(secondNumber);
        }
    }
}

