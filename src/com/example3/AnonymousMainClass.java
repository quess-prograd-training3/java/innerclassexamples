package com.example3;

public class AnonymousMainClass {
    public static void main(String[] args) {
        PersonClass personClassObject = new PersonClass() {
            @Override
            void display() {
                System.out.println("Hello World");
                System.out.println(var);
            }
        };
        personClassObject.display();
    }
}
